<?php
define( 'YOURLS_ADMIN', true );
define( 'YOURLS_AJAX', true );
require_once( dirname( __DIR__ ) .'/includes/load-yourls.php' );
yourls_maybe_require_auth();

// This file will output a JSON string
yourls_content_type_header( 'application/json' );

if( !isset( $_REQUEST['action'] ) )
	die();

// Pick action
$action = $_REQUEST['action'];
switch( $action ) {

	case 'add':
		yourls_verify_nonce( 'add_url', $_REQUEST['nonce'], false, 'omg error' );
		function new_collabora_yourls_link( $args ) {
			global $ydb;
			if (!$ydb->query('SELECT * FROM `yourls_url_owner`')) {
				$ydb->query('CREATE TABLE `yourls_url_owner` (
					`keyword` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
					`owner` text CHARACTER SET utf8,
					PRIMARY KEY (`keyword`)
				);');
			}
			$ydb->query('DELETE FROM `yourls_url_owner` WHERE `keyword`=\''.yourls_escape($args[1]).'\';');
			$ydb->query('INSERT INTO `yourls_url_owner` VALUES (\''.yourls_escape($args[1]).'\',\''.yourls_escape(YOURLS_USER).'\');');
		}
		yourls_add_action( 'post_add_new_link', 'new_collabora_yourls_link' );
		$return = yourls_add_new_link( $_REQUEST['url'], $_REQUEST['keyword'] );
		echo json_encode($return);
		break;
		
	case 'edit_display':
		yourls_verify_nonce( 'edit-link_'.$_REQUEST['id'], $_REQUEST['nonce'], false, 'omg error' );
		$row = yourls_table_edit_row ( $_REQUEST['keyword'] );
		echo json_encode( array('html' => $row) );
		break;

	case 'edit_save':
		yourls_verify_nonce( 'edit-save_'.$_REQUEST['id'], $_REQUEST['nonce'], false, 'omg error' );
		$return = yourls_edit_link( $_REQUEST['url'], $_REQUEST['keyword'], $_REQUEST['newkeyword'], $_REQUEST['title'] );
		echo json_encode($return);
		break;
		
	case 'delete':
		yourls_verify_nonce( 'delete-link_'.$_REQUEST['id'], $_REQUEST['nonce'], false, 'omg error' );
		$query = yourls_delete_link_by_keyword( $_REQUEST['keyword'] );
		echo json_encode(array('success'=>$query));
		break;
		
	case 'logout':
		// unused for the moment
		yourls_logout();
		break;
		
	default:
		yourls_do_action( 'yourls_ajax_'.$action );

}

die();
